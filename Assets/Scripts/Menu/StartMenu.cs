﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartMenu : MonoBehaviour
{
    //Variable informing about type of loading the game
    public static bool loaded=false;
    private void Start()
    {
        loaded = Loaded.instance.loaded;
    }

    //Function to start new game - load new scene
    public void StartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    //Function to exit game
    public void QuitGame()
    {
        Application.Quit();
    }

    //Function to load game from start menu
    public void LoadGame()
    {
        //Load new scene
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        //Create and change value in instance
        Loaded.instance.loaded = true;
        //Save load instance with information
        DontDestroyOnLoad(Loaded.instance);
    }
}
