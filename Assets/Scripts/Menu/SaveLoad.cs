﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class SaveLoad : MonoBehaviour
{
    Monitor_information statusInformation;
    GameObject coalStorage, cartonStorage, boxStorage, packageTable, box, truck, bowlmass;
    GameObject coal, carton, deliveryBox, package, cookie, player;
    GameObject[] packages;

    void Start()
    {
        try
        {
            //Loading resources materials
            coal = Resources.Load<GameObject>("coal") as GameObject;
            carton = Resources.Load<GameObject>("carton") as GameObject;
            deliveryBox = Resources.Load<GameObject>("delivery_box") as GameObject;
            package = Resources.Load<GameObject>("package") as GameObject;
            cookie = Resources.Load<GameObject>("cookieSpawned") as GameObject;


            //Finding objects to save
            player = GameObject.FindGameObjectWithTag("Player");
            truck = GameObject.Find("truck");
            coalStorage = GameObject.FindGameObjectWithTag("coalStorage");
            cartonStorage = GameObject.FindGameObjectWithTag("tableCarton");
            boxStorage = GameObject.FindGameObjectWithTag("boxStorage");
            packageTable = GameObject.FindGameObjectWithTag("tablePackage");
            box = GameObject.FindGameObjectWithTag("box");
            bowlmass = GameObject.Find("bowlmass");
            FindAllStatusInformations();

            //Check type of loading
            if (Loaded.instance.loaded == true)
            {
                Destroy(Loaded.instance.gameObject);
                loadGame();
            }
        }
        catch (NullReferenceException exp)
        {
            Debug.Log(exp);
        }

    }

    //Function to save objects values to XLM file
    public void saveGame()
    {
        //Create file to save
        FileStream file = File.Create(Application.persistentDataPath + "/saveFile.txt");
        PlayerData data = new PlayerData();

        //Assigning values to save
        data.money = Base.instance.money;
        data.score = Base.instance.score;
        data.playerPosition = player.transform.position;
        data.truckPoints = truck.GetComponent<Truck>().truckPoints;
        data.truckPackageCount = truck.GetComponent<Truck>().packageCount;
        data.massLvl = bowlmass.GetComponent<BowlMass>().level;

        //Assigning storages ammount
        savePackages(data);
        saveBox(data);
        saveItemBase(data);
        saveAllStorages(data);

        //Save values to file
        XmlSerializer xmlSerializer = new XmlSerializer(typeof(PlayerData));
        xmlSerializer.Serialize(file, data);
        file.Close();
    }

    //Function to load game - load saved values fro XLM file
    public void loadGame()
    {
        //Try to open file
        if (File.Exists(Application.persistentDataPath + "/saveFile.txt"))
        {
            FileStream file = File.Open(Application.persistentDataPath + "/saveFile.txt", FileMode.Open);
           
            //Get values from file and close file
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(PlayerData));
            PlayerData data = xmlSerializer.Deserialize(file) as PlayerData;
            file.Close();

            //Assigning saved values to actually objects
            Base.instance.money = data.money;
            Base.instance.score = data.score;
            player.transform.position = data.playerPosition;
            truck.GetComponent<Truck>().truckPoints = data.truckPoints;
            truck.GetComponent<Truck>().packageCount = data.truckPackageCount;
            bowlmass.GetComponent<BowlMass>().level = data.massLvl;
            bowlmass.GetComponent<BowlMass>().changeSize(data.massLvl);

            //Load items lists from Base
            loadItemBase(data);

            //Clear all storages to spawn new items
            clearStorages();

            //Spawning new items to storage in saved values
            loadStorage(data, data.coalStorage, GameObject.Find("spawnCoal"), coal);
            loadStorage(data, data.cartonStorage, GameObject.Find("spawnCarton"), carton);
            loadStorage(data, data.boxStorage, GameObject.Find("spawnIngredient"), deliveryBox);
            loadPackages(data);
            loadBox(data);

            //Updating displayed values in computer
            updateStatus();

            //Reset all NPC animations
            resetAnimations();

            //Unpause menu
            Menu menu = GameObject.FindGameObjectWithTag("interface").GetComponent<Transform>().GetComponentInChildren<Menu>();
            menu.GoStart();
        }


    }
    //Function to find all monitors with informations (cost/money/score)
    private void FindAllStatusInformations()
    {
        try
        {
            //Find all monitor informations 
            Monitor_information[] statusInformations = FindObjectsOfType<Monitor_information>() as Monitor_information[];
            //Iterate from list and add curret objects to list
            foreach (var stat in statusInformations)
            {
                if (stat.tag == "information")
                {
                    statusInformation = stat;
                    break;
                }
            }
        }
        catch (NullReferenceException exp)
        {
            Debug.Log(exp);
        }

    }

    //Function to update all monitors informations to actually values
    void updateStatus()
    {
        //Execute event to update monitor information status
        ExecuteEvents.Execute<UpdateEventMenager>(statusInformation.gameObject, null, (x, y) =>
        {
            x.UpdateStatus("money_inf_text");
            x.UpdateStatus("score_inf_text");
            x.UpdateStatus("cost_inf_text");
            x.UpdateStatus("truck_inf_text");
        });
    }

    //Function to save all item values (list) from Base
    void saveItemBase(PlayerData data)
    {
        //Iterate from Base item list and add all values to data
        foreach (var item in Base.instance.itemList)
        {
            data.itemLvl.Add(item.lvl);
            data.updateCosts.Add(item.updateCost);

        }
    }

    //Function to load all item values (list) from Base
    void loadItemBase(PlayerData data)
    {
        //Set new values to all Base item list
        for (int i = 0; i <= data.itemLvl.Count - 1; i++)
        {
            Base.instance.itemList[i].lvl = data.itemLvl[i];
            Base.instance.itemList[i].updateCost = data.updateCosts[i];
        }
    }

    //Function to save all storages ammount
    void saveAllStorages(PlayerData data)
    {
        data.coalStorage = coalStorage.GetComponent<CoalStorage>().coals.Count;
        data.cartonStorage = cartonStorage.GetComponent<CartonTable>().cartons.Count;
        data.boxStorage = boxStorage.GetComponent<BoxStorage>().boxes.Count;
;    }

    //Function to spawn items to storage in saved ammount
    void loadStorage(PlayerData data, int itemStorage, GameObject spawnPoint, GameObject item)
    {
        GameObject[] storage;

        try
        {
            //Find storage
            storage = GameObject.FindGameObjectsWithTag(item.tag);

            //Clear storage from objects
            for (int i = 0; i < storage.Length; i++)
            {
                Destroy(storage[i].gameObject);
            }

            //spawn new items
            for (int j = 0; j < itemStorage; j++)
            {
                Instantiate(item, spawnPoint.transform.position, spawnPoint.transform.rotation);
            }
        }
        catch (NullReferenceException exp)
        {
            Debug.Log(exp);
        }

    }

    //Function to clear storages from item lists
    void clearStorages()
    {
        //clear only items lists
        coalStorage.GetComponent<CoalStorage>().coals.Clear();
        cartonStorage.GetComponent<CartonTable>().cartons.Clear();
        boxStorage.GetComponent<BoxStorage>().boxes.Clear();
        packageTable.GetComponent<PackageTable>().packages.Clear();
    }

    //Function to reset all NPS's animations
    void resetAnimations()
    {
        
        GameObject[] workers;        
        try
        {
            //Find all workers
            workers = GameObject.FindGameObjectsWithTag("worker");

            //Set waiting to all workers
            for (int i = 0; i < workers.Length; i++)
            {
                workers[i].GetComponentInParent<Animator>().Play("waiting");
                workers[i].GetComponentInParent<Animator>().SetBool("havePackage", false);
            }

            //Set waiting to truck
            truck.GetComponent<Animator>().Play("waiting");

            //Check truck, if truck is full let him ride 
            if (truck.GetComponent<Truck>().packageCount != truck.GetComponent<Truck>().truckSize)
            {
                truck.GetComponent<Animator>().SetBool("ride", false);
            }
        }
        catch(NullReferenceException exp)
        {
            Debug.Log(exp);
        }

    }

    //Function to save ammount cookies from box
    void saveBox(PlayerData data)
    {
        List<double> boxList;
        try
        {
            boxList = box.GetComponent<Box>().points;
            //Assigment box values to data
            data.cookiesInBox = boxList;
            data.resizeCookies = box.GetComponent<Box>().resizeCookies;
        }
        catch (NullReferenceException exp)
        {
            Debug.Log(exp);
        }

    }

    //Function to load cookies ammount in saved box and spawn cookies
    void loadBox(PlayerData data)
    {
        try
        {
            //Find cookieSpawn in Scene
            GameObject cookieSpawn = GameObject.FindGameObjectWithTag("cookieSpawn");
            StartCoroutine(ClearBox());
            StartCoroutine(SpawnNewCookies());
            StartCoroutine(SetLoadedItems());

            //Coroutine to clear all cookies from box (list and objects)
            IEnumerator ClearBox()
            {
                for (int i = 0; i < box.GetComponent<Box>().items.Count; i++)
                {
                    if (box.GetComponent<Box>().items.Count > 0)
                        Destroy(box.GetComponent<Box>().items.ElementAt(i).gameObject);
                }
                box.GetComponent<Box>().items.Clear();
                yield return new WaitForSeconds(1);
            }

            //Coroutine to spawn new cookies in saved ammount
            IEnumerator SpawnNewCookies()
            {
                for (int j = 0; j < data.cookiesInBox.Count; j++)
                {
                    Instantiate(cookie, cookieSpawn.transform.position, cookieSpawn.transform.rotation);
                }
                yield return new WaitForSeconds(1);
            }

            //Coroutine to set saved lists values to loaded box's lists
            IEnumerator SetLoadedItems()
            {
                box.GetComponent<Box>().points = data.cookiesInBox;
                box.GetComponent<Box>().resizeCookies = data.resizeCookies;
                yield return new WaitForSeconds(1);
            }
        }
        catch (NullReferenceException exp)
        {
            Debug.Log(exp);
        }

    }

    //Function to save ammount packages, package's levels/sizes
    void savePackages(PlayerData data)
    {
        try
        {
            //Find all packages
            GameObject[] packages = GameObject.FindGameObjectsWithTag("package");

            //save packages ammount
            data.packages = packages.Length;

            //save packages sizes 
            for (int i = 0; i < packages.Length; i++)
            {
                data.packageSizes.Add(packages[i].GetComponent<Package>().packageSize);
                data.packagesPoint.Add(packages[i].GetComponent<Package>().packagePoints);
                data.cookiesInPackages.Add(packages[i].GetComponent<Package>().cookieCount);
            }
        }
        catch (NullReferenceException exp)
        {
            Debug.Log(exp);
        }



    }

    //Function to load saved packages ammount and spawn packages
    void loadPackages(PlayerData data)
    {
        try
        {
            StartCoroutine(spawnPackages());
            StartCoroutine(setPackageLevels());

            //Coroutine to spawn packages int package storage
            IEnumerator spawnPackages()
            {
                loadStorage(data, data.packages, GameObject.FindGameObjectWithTag("spawnPackage"), package);
                yield return new WaitForSeconds(0.1f);
            }

            //Coroutine to set new sizes to spawned packages
            IEnumerator setPackageLevels()
            {
                yield return new WaitForSeconds(2);
                packages = GameObject.FindGameObjectsWithTag("package");
                for (int i = 0; i < data.packages; i++)
                {

                    packages[i].GetComponent<Package>().packageSize = data.packageSizes.ElementAt(i);
                    packages[i].GetComponent<Package>().cookieCount = data.cookiesInPackages.ElementAt(i);
                    packages[i].GetComponent<Package>().packagePoints = data.packagesPoint.ElementAt(i);
                }
            }
        }
        catch (NullReferenceException exp)
        {
            Debug.Log(exp);
        }

    }

}

