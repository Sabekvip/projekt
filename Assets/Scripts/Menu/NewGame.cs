﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class NewGame : MonoBehaviour
{
    //Function to start new game
    public void StartNewGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        Time.timeScale = 1;
    }

}

