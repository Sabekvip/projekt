﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndGame //: MonoBehaviour
{
    double points;

    //function for ending the game that is not possible
    public bool endPoint()
    {
        bool endgame= false;
        bool emptyBoxStorage = false;
        bool emptyCartonStorage = false;
        bool emptyCoalStorage = false;

        FindAllPoints();

        //check ingredient storage
        if (GameObject.FindGameObjectWithTag("boxStorage").GetComponent<BoxStorage>().emptyBoxStorage==true)
        {
            if (GameObject.FindGameObjectWithTag("bowlmass").GetComponent<BowlMass>().level == 0)
            {
                if (Base.instance.money+points < Base.instance.deliversList[2].deliveryCost)
                {
                    emptyBoxStorage = true;
                }
                else emptyBoxStorage = false;
            }
        }

        //check carton storage
        if (GameObject.FindGameObjectWithTag("tableCarton").GetComponent<CartonTable>().emptyCartonTable == true)
        {
            if (GameObject.FindGameObjectWithTag("tablePackage").GetComponent<PackageTable>().emptyPackageTable == true)
            {
                if (Base.instance.money + points < Base.instance.deliversList[1].deliveryCost)
                {
                    emptyCartonStorage = true;
                }
                else emptyCartonStorage = false;
            }
        }

        //check coal storage
        if (GameObject.FindGameObjectWithTag("coalStorage").GetComponent<CoalStorage>().emptyCoalStorage == true)
        {
            if (Base.instance.money + points < Base.instance.deliversList[0].deliveryCost)
            {
                emptyCoalStorage = true;
            }
            else emptyCoalStorage = false;
        }

        if ((emptyBoxStorage == true && emptyCartonStorage == true) || 
            (emptyBoxStorage == true && emptyCoalStorage == true))
        {
            endgame = true;
        }
        else endgame = false;
        points = 0;
        return endgame;

        
    }


    //Function to find all point from packages
    void FindAllPoints()
    {
        GameObject[] packages = GameObject.FindGameObjectsWithTag("package");

        for (int i = 0; i < packages.Length; i++)
        {
            points += packages[i].GetComponent<Package>().packagePoints;
        }
        points += GameObject.Find("truck").GetComponent<Truck>().truckPoints;
    }
}
