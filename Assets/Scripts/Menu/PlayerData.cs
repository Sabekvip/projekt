﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using UnityEngine;


//Data to save
[Serializable]
public class PlayerData
{
    public String sceneName;
    [DataMember]
    public Vector3 playerPosition;
    public double money;
    public double score;
    public double truckPoints;
    public double resizeCookies;
    public int truckPackageCount;
    public int coalStorage;
    public int cartonStorage;
    public int boxStorage;
    public int packages;
    public int massLvl;


    public List<int> packageSizes = new List<int>();
    public List<double> packagesPoint = new List<double>();
    public List<int> cookiesInPackages = new List<int>();
    public List<int> itemLvl = new List<int>();
    public List<double> updateCosts = new List<double>();
    public List<double> cookiesInBox = new List<double>();

}


