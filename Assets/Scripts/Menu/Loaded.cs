﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Loaded : MonoBehaviour
{
    //Instance specifying the type of loading the game
    private static Loaded _instance;
    public static Loaded instance
    {
        get
        {
            if (_instance == null)
            {
                GameObject itemBase = new GameObject("loaded");
                itemBase.AddComponent<Loaded>();
            }
            return _instance;
        }
    }

    public bool loaded;


    public void Awake()
    {
        _instance = this;
        loaded = false;
    }
}
