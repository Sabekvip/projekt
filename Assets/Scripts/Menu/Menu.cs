﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{
    public Canvas mainMenu;
    public Canvas quitMenu;
    public Canvas endGameMenu;
    public Canvas loadMenu;
    public Canvas saveMenu;
    public Canvas newGame;
    public Canvas instuction;
    public Canvas startGameMenu;
    public Button startButton;
    public Button loadButton;
    public Button saveButton;
    public Button newGameButton;
    public Button exitButton;
    public Button instructionButton;
    public Text Score;


    EndGame end = new EndGame();
    // Start is called before the first frame update
    void Start()
    {

        //Get curret canvas component
        mainMenu = mainMenu.GetComponent<Canvas>();
        quitMenu = quitMenu.GetComponent<Canvas>();
        endGameMenu = endGameMenu.GetComponent<Canvas>();
        loadMenu = loadMenu.GetComponent<Canvas>();

        //Get curret button component
        startButton = startButton.GetComponent<Button>();
        loadButton = loadButton.GetComponent<Button>();
        saveButton = saveButton.GetComponent<Button>();
        newGameButton = newGameButton.GetComponent<Button>();
        exitButton = exitButton.GetComponent<Button>();
        instructionButton = instructionButton.GetComponent<Button>();

        //Set all canvas disable
        mainMenu.enabled = false;
        quitMenu.enabled = false;
        loadMenu.enabled = false;
        saveMenu.enabled = false;
        endGameMenu.enabled = false;
        instuction.enabled = false;
        newGame.enabled = false;
        Time.timeScale = 0;
        Cursor.lockState = CursorLockMode.Confined;


    }

    // Update is called once per frame
    void Update()
    {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (quitMenu.enabled == true) quitMenu.enabled = false;
                if (loadMenu.enabled == true) loadMenu.enabled = false;
                if (saveMenu.enabled == true) saveMenu.enabled = false;
                if (newGame.enabled == true) newGame.enabled = false;




            mainMenu.enabled = !mainMenu.enabled;
                if (mainMenu.enabled)
                {
                    Time.timeScale = 0;
                    Cursor.lockState = CursorLockMode.Confined;
                }
                else
                {
                    Time.timeScale = 1;
                    Cursor.lockState = CursorLockMode.Locked;
                }
            
    }

    CheckEnd();
    }
    //Function to close menu
    public void GoStart()
    {
        mainMenu.enabled = false;
        Time.timeScale = 1;
        Cursor.lockState = CursorLockMode.Locked;
        
    }
    //Function to enable quit game menu
    public void QuitGame()
    {
        quitMenu.enabled = true;
    }

    //Function to quit game
    public void YesQuit()
    {
        Application.Quit();
    }

    //Function to close quit game
    public void NoQuit()
    {
        quitMenu.enabled = false;
        EnableMenuButtons();
    }
    
    //Function to enable load game menu
    public void LoadGame()
    {
        loadMenu.enabled = true;
    }

    //Function to close all menu and load game - to use with SaveLoad.loadGame
    public void YesLoadGame()
    {
        loadMenu.enabled = false;
        mainMenu.enabled = false;
        startGameMenu.enabled = false;
        EnableMenuButtons();
    }

    //Function to close load menu
    public void NoLoadGame()
    {
         loadMenu.enabled = false;
         EnableMenuButtons();
    }

    //Function to load close end game menu and load game - to use with SaveLoad.loadGame
    public void LoadGameFromEnd()
    {
        endGameMenu.enabled = false;
        Time.timeScale = 1;
        Cursor.lockState = CursorLockMode.Locked;

    }

    //Function to cloase end game menu and start new game - to use with NewGame.StartNewGame
    public void NewGameFromEnd()
    {
        Time.timeScale = 1;
        Cursor.lockState = CursorLockMode.Locked;
        endGameMenu.enabled = false;
    }

    //Function to enable save menu
    public void SaveGame()
    {
        saveMenu.enabled = true;
    }

    //Function to save/or not save game - to use with SaveLoad.saveGame
    public void YesOrNoSaveGame()
    {
        saveMenu.enabled = false;
    }

    //Function to enable intruction menu and disable main menu
    public void Instruction()
    {
        instuction.enabled = true;
        mainMenu.enabled = false;
    }

    //Function to disable intruction menu and enable main menu
    public void CloseInstruction()
    {
        instuction.enabled = false;
        mainMenu.enabled = true;
    }

    //Function to enable new game menu
    public void NewGame()
    {
        newGame.enabled = true;
    }

    //Function to start new game
    public void YesNewGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    //Function to close new game menu
    public void NoNewGame()
    {

        newGame.enabled = false;
        EnableMenuButtons();  
    }
    //Function to check end game
    public void CheckEnd()
    {   

        if (end.endPoint() == true)
        {
            EndGame();
        }
    }
    //Function enable end game menu with score
    public void EndGame()
    {
            Time.timeScale = 0;
            Cursor.lockState = CursorLockMode.Confined;
            endGameMenu.enabled = true;
            Score.GetComponent<Text>().text = ("Score: " + Base.instance.score);
            mainMenu.enabled = false;

    }

    //Function to disable all menu buttons
    public void DisableMenuButtons()
    {
        startButton.enabled = false;
        loadButton.enabled=false;
        saveButton.enabled = false;
        newGameButton.enabled = false;
        exitButton.enabled=false;
        instructionButton.enabled = false;
    }

    //Function to enable all menu buttons
    public void EnableMenuButtons()
    {
        startButton.enabled = true;
        loadButton.enabled = true;
        saveButton.enabled = true;
        newGameButton.enabled = true;
        exitButton.enabled = true;
        instructionButton.enabled = true;
    }
}
