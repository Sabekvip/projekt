﻿using UnityEngine;
using UnityEngine.UI;


public class Monitor_information : MonoBehaviour, UpdateEventMenager
{
    public Text text_information;
    GameObject obj;
    string objTag;
    Truck truck;
    GameObject deliveryTruck;
    void Start()
    {
        deliveryTruck = GameObject.FindGameObjectWithTag("truck");
        truck = deliveryTruck.GetComponent<Truck>();
        text_information = GetComponent<Text>();
    }


    //Function to update status on monitor information objects by new value
    public void UpdateStatus(string objectTag)
    {
        this.objTag = objectTag;
        obj = GameObject.Find(objTag);

        if (obj.name == "score_inf_text")
        {
            obj.GetComponent<Text>().text = Base.instance.score.ToString();
        }

        if (obj.name == "money_inf_text")
        {
            obj.GetComponent<Text>().text = Base.instance.money.ToString();
        }

        if (obj.name == "cost_inf_text")
        {
            if (Base.instance.upgradeOption == true)
            {
                obj.GetComponent<Text>().text = Base.instance.itemList[Base.instance.upgradePosition].updateCost.ToString();
            }
            if (Base.instance.deliveryOption == true)
            {
                obj.GetComponent<Text>().text = Base.instance.deliversList[Base.instance.deliveryPosition].deliveryCost.ToString();
            }
        }
        if (obj.name == "truck_inf_text")
        {
            obj.GetComponent<Text>().text = truck.truckPoints.ToString();
        }
    }
}
