﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class changeColorEvent : MonoBehaviour
{

    private void OnEnable()
    {
        Operation.changeOptionColor += colorUpdate;
    }
    private void OnDisable()
    {
        Operation.changeOptionColor -= colorUpdate;
    }

    void colorUpdate()
    {

        colorUpdate(Base.instance.cameraOption, "camera_inf_color");
        colorUpdate(Base.instance.upgradeOption, "upgrade_inf_color");
        colorUpdate(Base.instance.deliveryOption, "delivery_inf_color");
    }

    public void colorUpdate(bool objectOption, string objecOptionInformation)
    {
        GameObject obj;
        obj = GameObject.Find(objecOptionInformation);
        if (objectOption == true)
        {
            obj.GetComponent<MeshRenderer>().material.color = Color.green;
        }
        else obj.GetComponent<MeshRenderer>().material.color = Color.red;
    }
}
