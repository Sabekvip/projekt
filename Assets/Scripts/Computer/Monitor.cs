﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Monitor : MonoBehaviour
{
    public Texture[] upgradeItemsTextures;
    public Texture[] deliveryTextures;

    public Texture cameraOptionTexture;
   
    //Function to change texture on monitor - using in camera optpion
    public void changeTexture(GameObject obj, Texture texture)
    {
        obj.GetComponent<Renderer>().material.mainTexture = texture;
    }

    //Function to change texture on monitor - using in upgrade option
    public void changeUpgradeTexture(GameObject obj, Texture[] textures)
    {
        obj.GetComponent<Renderer>().material.mainTexture = upgradeItemsTextures[Base.instance.upgradePosition];
    }

    //Function to change texture on monitor - using in delivery option
    public void changeDeliveryTexture(GameObject obj, Texture[] textures)
    {
        obj.GetComponent<Renderer>().material.mainTexture = deliveryTextures[Base.instance.deliveryPosition];
    }
}
