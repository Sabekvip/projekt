﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Operation : MonoBehaviour
{
    Ray ray;
    RaycastHit hit;
    public Camera cam;

    GameObject[] cameras;
    List<Item> items;
    List<Delivers> delivers;

    Monitor_information statusInformation;
    Monitor monitor;

    //Controller to spawn event
    SpawnController spawnPoint;


    public Transform SpawnPoint;
    public GameObject prefab;
    BowlMass bowlMass;
    GameObject mass;
    MassTable massTable;
    GameObject table;

    //Truck animator
    [SerializeField] public Animator animTruck;


    int cameraPosition;
    int upgradePosition;
    int deliveryPosition;


    public delegate void changeColorEv();
    public static event changeColorEv changeOptionColor;


    void Start()
    {
        //Setting instances
        mass = GameObject.Find("bowlmass");
        bowlMass = mass.GetComponent<BowlMass>();
        table = GameObject.Find("table");
        massTable = table.GetComponentInChildren<MassTable>();

        //Find all needed objects
        FindAllStatusInformations();
        FindAllMonitors();
        FindAllSpawnPoints();
        
        cameraPosition = Base.instance.cameraPosition;
        upgradePosition = Base.instance.upgradePosition;
        deliveryPosition = Base.instance.deliveryPosition;
        
        items = Base.instance.itemList;
        cameras = Base.instance.cameras;
        delivers = Base.instance.deliversList;

        //Disable all cameras on start game
        disabeAllCameras();

        //Update money information
        UpdateInformation("money");

    }
    private void FixedUpdate()
    {
        ray = cam.ScreenPointToRay(Input.mousePosition);
    }

    void Update()
    {
        if (Physics.Raycast(ray, out hit, 5))
        {
            if (Input.GetMouseButtonDown(0))
            {
                //Checking hit to camera button and change section
                if (hit.collider.tag == "cameraSectionButton")
                {
                    Base.instance.cameraOption = true;
                    Base.instance.upgradeOption = false;
                    Base.instance.deliveryOption = false;
                    monitor.changeTexture(monitor.gameObject, monitor.cameraOptionTexture);
                    //cameraPosition = 1;
                    cameras[cameraPosition].SetActive(true);

                    if (changeOptionColor != null)
                    {
                        changeOptionColor();
                    }

                }
                //Checking hit to upgrade button and change section
                if (hit.collider.tag == "upgradeSectionButton")
                {
                    
                    Base.instance.cameraOption = false;
                    Base.instance.upgradeOption = true;
                    Base.instance.deliveryOption = false;
                    Base.instance.upgradePosition = 0;
                    monitor.changeTexture(monitor.gameObject, monitor.upgradeItemsTextures[Base.instance.upgradePosition]);
                    UpdateInformation("cost");
                    if (changeOptionColor != null)
                    {
                        changeOptionColor();
                    }
                    disabeAllCameras();
                }
                //Checking hit to delivery button and change section
                if (hit.collider.tag == "deliverySectionButton")
                {
                    Base.instance.cameraOption = false;
                    Base.instance.upgradeOption = false;
                    Base.instance.deliveryOption = true;
                    monitor.changeTexture(monitor.gameObject, monitor.deliveryTextures[Base.instance.deliveryPosition]);
                    UpdateInformation("cost");
                    if (changeOptionColor != null)
                    {
                        changeOptionColor();
                    }
                    disabeAllCameras();
                }

                //Carry on camera option functions
                if (Base.instance.cameraOption == true)
                {
                    cameraOperation();
                }

                //Carry on upgrade option functions
                if (Base.instance.upgradeOption == true)
                {
                    upgradeOperation();
                }

                //Carry on delivery option functions
                if (Base.instance.deliveryOption == true)
                {
                    deliveryOperation();
                }


                //Check hit to spawn button
                if (hit.collider.tag == "SpawnButton")
                {
                    //Check avalilable to spawn mass
                    if (bowlMass.level > 0 && massTable.massOnTable == false)
                    {
                        bowlMass.level--;
                        bowlMass.changeSize(bowlMass.level);
                        Instantiate(prefab, SpawnPoint.position, SpawnPoint.rotation);
                        //Disable spawn function 
                        if (bowlMass.level < 7)
                        {
                            bowlMass.animWorker.SetBool("fullBowlLevel", false);
                        }
                    }
                    
                }
                //Check hit to truck button
                if (hit.collider.tag == "truckButton")
                {
                    animTruck.SetBool("ride", true);
                }
            }
        }
    }

    //Function to choose camera
    void cameraOperation()
    {
        //Next camera
        if (hit.collider.tag == "nextButton")
        {
            cameras[cameraPosition].SetActive(false);
            cameraPosition++;
            if (cameraPosition > cameras.Length - 1)
            {
                cameraPosition = 0;
            }
            cameras[cameraPosition].SetActive(true);
        }
        //Previous camera
        if (hit.collider.tag == "previousButton")
        {
            cameras[cameraPosition].SetActive(false);
            cameraPosition--;
            if (cameraPosition < 0)
            {
                cameraPosition = cameras.Length - 1;
            }
            cameras[cameraPosition].SetActive(true);
        }
    }
    //Function to disable all cameras
    void disabeAllCameras()
    {
        foreach(GameObject cam in cameras)
        {
            cam.SetActive(false);
        }
    }
    //Function to choose item to upgrade
    void upgradeOperation()
    {
        //Next item to upgrade with update cost information
        if (hit.collider.tag == "nextButton")
        {
            upgradePosition++;
            
            if (upgradePosition > items.Count-1)
            {
                upgradePosition = 0;
            }
            Base.instance.upgradePosition = upgradePosition;
            UpdateInformation("cost");
        }

        //Previous item to upgrade with update cost information
        if (hit.collider.tag == "previousButton")
        {
            upgradePosition--;
            if (upgradePosition < 0)
            {
                upgradePosition = items.Count - 1;
            }
            Base.instance.upgradePosition = upgradePosition;
            UpdateInformation("cost");
        }

        //Upgrade curret item
        if (hit.collider.tag == "acceptButton")
        {
            
            acceptUpgrade();
            UpdateInformation("cost");
            UpdateInformation("money");
        }
        monitor.changeTexture(monitor.gameObject, monitor.upgradeItemsTextures[upgradePosition]);
    }

    //Function to upgrade item
    void acceptUpgrade()
    {
        //Check avalilable (money) to upgrade curret item
        if (Base.instance.money >= items[upgradePosition].updateCost)
        {
            Base.instance.money = Base.instance.money - items[upgradePosition].updateCost;
            items[upgradePosition].lvl++;
            items[upgradePosition].updateCost = items[upgradePosition].lvl * items[upgradePosition].multipler;
        }
        if(upgradePosition==1 && items[upgradePosition].lvl >= 17)
        {
            items[upgradePosition].updateCost = 999999999;
        }
    }

    //Function to update monitor information - execute events
    void UpdateInformation(string status)
    {
        if (status == "money")
        {
            ExecuteEvents.Execute<UpdateEventMenager>(statusInformation.gameObject, null, (x, y) =>
            {
                x.UpdateStatus("money_inf_text");
            });
        }
        if (status == "cost")
        {
            ExecuteEvents.Execute<UpdateEventMenager>(statusInformation.gameObject, null, (x, y) =>
            {
                x.UpdateStatus("cost_inf_text");
            });
        }
        if (status == "score")
        {
            ExecuteEvents.Execute<UpdateEventMenager>(statusInformation.gameObject, null, (x, y) =>
            {
                x.UpdateStatus("score_inf_text");
            });
        }
    }

  
    //Function to spawn item - execute events
    void SpawnItem(string item)
    {
        ExecuteEvents.Execute<SpawnEventMenager>(spawnPoint.gameObject, null, (x, y) =>
        {
            x.SpawnItem(item);
        });
    }

    //Funtion to choose delivery
    void deliveryOperation()
    {
        //Next delivery
        if (hit.collider.tag == "nextButton")
        {
            
            deliveryPosition++;
            if (deliveryPosition > delivers.Count - 1)
            {
                deliveryPosition = 0;
            }
            
            Base.instance.deliveryPosition = deliveryPosition;
            UpdateInformation("cost");
        }

        //Previous delivery
        if (hit.collider.tag == "previousButton")
        {
            deliveryPosition--;
            if (deliveryPosition < 0)
            {
                deliveryPosition = delivers.Count - 1;
            }
            
            Base.instance.deliveryPosition = deliveryPosition;
            UpdateInformation("cost");
        }

        //Check avalilable to delivery items(money) and spawn items in curret storage
        if (hit.collider.tag == "acceptButton")
        {
            if (Base.instance.money >= delivers[deliveryPosition].deliveryCost)
            {
                Base.instance.money = Base.instance.money-delivers[deliveryPosition].deliveryCost;
                UpdateInformation("money");
                SpawnItem(delivers[deliveryPosition].deliveryName);
            } 
        }
        monitor.changeTexture(monitor.gameObject, monitor.deliveryTextures[Base.instance.deliveryPosition]);
    }

    //Function to find all monitor informations
    public void FindAllStatusInformations()
    {
        Monitor_information[] statusInformations = FindObjectsOfType<Monitor_information>() as Monitor_information[];
        foreach (var stat in statusInformations)
        {
            if (stat.tag == "information")
            {
                statusInformation = stat;
                break;
            }
        }
    }

    //Function to find all monitors
    public void FindAllMonitors()
    {
        Monitor[] monitors = FindObjectsOfType<Monitor>() as Monitor[];
        foreach (var monit in monitors)
        {
            if (monit.tag == "monitor")
            {
                monitor = monit;
                break;
            }
        }
    }

    //Funtion to find all spawnPoints (in storages)
    public void FindAllSpawnPoints()
    {
        SpawnController[] points = FindObjectsOfType<SpawnController>() as SpawnController[];
        foreach (var point in points)
        {
            if (point.tag == "spawnPoint")
            {
                spawnPoint = point;
                break;
            }
        }
    }

}

