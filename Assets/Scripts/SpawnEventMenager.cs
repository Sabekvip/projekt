﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public interface SpawnEventMenager: IEventSystemHandler
{
    void SpawnItem(string itemName);
}
