﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[Serializable]
public class Base : MonoBehaviour
{
    private static Base _instance;
    public static Base instance
    {
        get
        {
            if (_instance == null)
            {
                GameObject itemBase = new GameObject("Base");
                itemBase.AddComponent<Base>();
            }
            return _instance;
        }
    }
    //List of items with upgrade avalilable
    public List<Item> itemList { get; set; } = new List<Item>();
    
    //List of delivers
    public List<Delivers> deliversList { get; set; } = new List<Delivers>();

    //Table with cameras
    public GameObject[] cameras;

    public double score { get; set; }
    public double money { get; set; }
    public bool cameraOption;
    public bool upgradeOption;
    public bool deliveryOption;
    public int cameraPosition;
    public int upgradePosition;
    public int deliveryPosition;

    private void Start()
    {
        
    }
    public void Awake()
    {
     

        //Items list
        //id | lvl | update cost | multipler
        _instance = this;
        itemList.Add(new Item(0, 1, 1, 7));    //cookie
        itemList.Add(new Item(1, 1, 1, 1000));    //cookie worker
        itemList.Add(new Item(2, 1, 1, 50));    //package  
        itemList.Add(new Item(3, 1, 1, 195));    //box 
        itemList.Add(new Item(4, 1, 1, 2500));    //truck

        //Delivers list
        deliversList.Add(new Delivers(0, "spawnCoal", 300));
        deliversList.Add(new Delivers(1, "spawnCarton", 1000));
        deliversList.Add(new Delivers(2, "spawnIngredient", 700));


        cameras = GameObject.FindGameObjectsWithTag("Camera");

        cameraPosition = 0;
        upgradePosition = 0;
        deliveryPosition = 0;

        cameraOption = false;
        upgradeOption = false;
        deliveryOption = false;

        money = 1000;
    }
}
