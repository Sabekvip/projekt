﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]

public class Delivers
{
    public int id;
    public string deliveryName;
    public double deliveryCost;

    Delivers() { }
    public Delivers(int id, string deliveryName, double deliveryCost)
    {
        this.id = id;
        this.deliveryName = deliveryName;
        this.deliveryCost = deliveryCost;
    }
}
