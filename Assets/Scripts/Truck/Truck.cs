﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Truck : MonoBehaviour
{
    [SerializeField] public Animator animTruck;

    public int truckLvl;
    public int packageCount;
    public int truckSize;
    public double truckPoints;
    public double updateCost;
    private Monitor_information statusInformation;
    GameObject tablePackage;

    void Start()
    {
        tablePackage = GameObject.FindGameObjectWithTag("tablePackage");
        truckLvl = Base.instance.itemList[4].lvl;
        Base.instance.itemList[4].updateCost = Base.instance.itemList[4].lvl* Base.instance.itemList[4].multipler;
        truckSize = truckLvl * 5;
        truckPoints = 0;

        FindAllStatusInformations();
        

    }

    void OnTriggerEnter(Collider collider)
    {
        if (collider.tag == "package")
        {
            //check fill truck
            if (packageCount <= truckSize)
            {
                packageCount++;
                //add points from package to truck
                truckPoints += collider.GetComponent<Package>().packagePoints;
                Destroy(collider.gameObject);

                //update truck monitor information
                ExecuteEvents.Execute<UpdateEventMenager>(statusInformation.gameObject, null, (x, y) =>
                {
                    x.UpdateStatus("truck_inf_text");
                });

                //Ride full track
                if (packageCount == truckSize)
                {
                    animTruck.SetBool("ride", true);
                    tablePackage.SetActive(false);
                    Ride();
                }
            }
        }
        if (collider.tag == "endRide")
        {
            animTruck.SetBool("ride", false);
            tablePackage.SetActive(true);
            Ride();
        }
    }

    
    //Function to find all Monitor information
    private void FindAllStatusInformations()
    {
        Monitor_information[] statusInformations = FindObjectsOfType<Monitor_information>() as Monitor_information[];
        foreach (var stat in statusInformations)
        {
            if (stat.tag == "information")
            {
                statusInformation = stat;
                break;
            }
        }
    }

    //Funtion to add points to player account, reset truck and update monitor informations
    void Ride()
    {
        Base.instance.money += truckPoints;
        Base.instance.score += truckPoints;
        truckPoints = 0;
        packageCount = 0;
        ExecuteEvents.Execute<UpdateEventMenager>(statusInformation.gameObject, null, (x, y) =>
        {
            x.UpdateStatus("score_inf_text");
            x.UpdateStatus("money_inf_text");
            x.UpdateStatus("truck_inf_text");
        });
    }
}
