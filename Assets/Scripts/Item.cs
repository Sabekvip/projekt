﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class Item
{
    public int id;
    public int lvl;
    public double updateCost;
    public int multipler;

    Item() { }
    public Item(int id, int lvl, double updateCost, int multipler)
    {
        this.id = id;
        this.lvl = lvl;
        this.updateCost = updateCost;
        this.multipler = multipler;
    }
}
