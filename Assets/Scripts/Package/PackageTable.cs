﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PackageTable : MonoBehaviour
{
    [SerializeField] public Animator animPackage;
    [SerializeField] public Animator animCarton;
    public List<GameObject> packages = new List<GameObject>();
    public bool emptyPackageTable = false;
    private void OnTriggerEnter(Collider collider)
    {
        if (collider.tag == "package")
        {
            //add package to package storage (table)
            packages.Add(collider.gameObject);
            animPackage.SetBool("packageOnTable", true);
            
            //check ammount for carton worker
            if (packages.Count >= 3) animCarton.SetBool("enoughPackages", true);
        }
        if (collider.tag == "worker")
        {
            if (animPackage.GetBool("havePackage") == false)
            {
                //Set last element
                int lastElement = packages.Count - 1;

                //Transform last element to worker
                packages.ElementAt(lastElement).transform.position = new Vector3(
                    (float)(collider.transform.position.x - 0.3),
                    collider.transform.position.y,
                    (float)(collider.transform.position.z + 0.8));

                //Set worker as parent
                packages.ElementAt(lastElement).transform.parent = collider.transform;

                //Freeze last element
                packages.ElementAt(lastElement).GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
                animPackage.SetBool("havePackage", true);
            }
        }
    }

    private void OnTriggerExit(Collider collider)
    {
        if (collider.tag == "package")
        {
            //remove package from table
            packages.Remove(collider.gameObject);

            //check empty table
            if (packages.Count == 0)
            {
                animPackage.SetBool("packageOnTable", false);
                emptyPackageTable = true;
            }
            else emptyPackageTable = false;

            //check ammount package on table for carton worker
            if (packages.Count<3) animCarton.SetBool("enoughPackages", false);

            for (int i = packages.Count - 1; i > -1; i--)
            {
                if (packages[i] == null)
                {
                    Destroy(packages.ElementAt(i).gameObject);
                    packages.RemoveAt(i);

                }
            }
        }
    }

}
