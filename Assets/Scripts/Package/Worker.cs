﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Worker : MonoBehaviour
{
    public GameObject placeToLeavePackage;

    public Animator anim;

    private void Start()
    {
        anim = GetComponent<Animator>();
        anim.SetBool("havePackage", false);
        anim.SetBool("haveFullPackage", false);
        anim.SetBool("packageOnTable", false);
    }
    private void Update()
    {
        Debug.Log("havePackage "+anim.GetBool("havePackage"));
        Debug.Log("haveFullPackage " + anim.GetBool("haveFullPackage"));
        Debug.Log("packageOnTable " + anim.GetBool("packageOnTable"));
    }

    void OnTriggerEnter(Collider collider)
    {

        if (collider.tag == "bandPackage")
        {
            //Put package to plase to leafe package
            transform.GetComponentInChildren<Package>().transform.position = placeToLeavePackage.transform.position;

            //Set worker a no parent
            transform.GetComponentInChildren<Package>().transform.SetParent(null);
        }  
    }

    private void OnTriggerStay(Collider collider)
    {
        if (collider.tag == "box")
        {
            //Check cookie ammount on package
            if (gameObject.GetComponentInChildren<Package>().cookieCount >= gameObject.GetComponentInChildren<Package>().packageSize)
            {
                anim.SetBool("haveFullPackage", true);
            }
        }
    }

    private void OnTriggerExit(Collider collider)
    {
            if (collider.tag == "bandPackage")
            {
                anim.SetBool("havePackage", false);
                anim.SetBool("haveFullPackage", false);
            }
    }
}
