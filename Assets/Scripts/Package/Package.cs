﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Package : MonoBehaviour
{

    public int packageLvl;
    public int cookieCount;
    public int packageSize;
    public double packagePoints;
    public double updateCost;
    void Start()
    {
        packageLvl = Base.instance.itemList[2].lvl;
        Base.instance.itemList[2].updateCost = Base.instance.itemList[2].lvl*Base.instance.itemList[2].multipler;
        packageSize = packageLvl * 14;
        cookieCount = 0;
    }

   
}
