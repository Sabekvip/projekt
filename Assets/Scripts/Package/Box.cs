﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using UnityEngine;

public class Box : MonoBehaviour
{
    public double resizeCookies;
    public List<GameObject> items = new List<GameObject>();
    public List<double> points = new List<double>();
    
    void Start()
    {
        Base.instance.itemList[3].updateCost = Base.instance.itemList[3].lvl* Base.instance.itemList[3].multipler;
        resizeCookies = Base.instance.itemList[3].lvl/10;
    }

    void Update()
    {
        resizeCookies = Base.instance.itemList[3].lvl * 0.005;
    }

    void OnTriggerEnter(Collider collider)
    {
        if (collider.tag == "Cookie")
        {
                //Add object to list
                items.Add(collider.gameObject);
                if (collider.GetComponent<Cookie>().points != 0)
                {
                    //Add cookie points to lsit
                    points.Add(collider.GetComponent<Cookie>().points);
                }
                //Changing the size of the cookie by box level
                collider.transform.localScale = new Vector3(
                (float)(collider.transform.localScale.x- resizeCookies), 
                (float)(collider.transform.localScale.y - resizeCookies), 
                (float)(collider.transform.localScale.z - resizeCookies));
        }
    }
    private void OnTriggerStay(Collider collider)
    {
        if (collider.tag == "package")
        {
            //Check avalilable to take cookies from box
            if (0 < points.Count && collider.GetComponent<Package>().cookieCount <= collider.GetComponent<Package>().packageSize) 
            {
                    //Add cookie points to package
                    collider.GetComponent<Package>().packagePoints += points.ElementAt(0);
                    //Reduce avalilable cookie count in package
                    collider.GetComponent<Package>().cookieCount++;
                    //Remove added points from box
                    points.RemoveAt(0);
                    //Destroy added item's point from box
                    Destroy(items.ElementAt(0).gameObject);
                    items.RemoveAt(0);

            }
        }
    }
    private void OnTriggerExit(Collider collider)
    {
        //checking and removing unnecessary cookies
        if (collider.tag == "Cookie")
        {
            items.Remove(collider.gameObject);
            points.Remove(collider.GetComponent<Cookie>().points);
        }

    }

}
