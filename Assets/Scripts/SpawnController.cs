﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnController : MonoBehaviour, SpawnEventMenager
{
    public GameObject spawnPoint;
    public GameObject spawnObject;
    GameObject obj;



    //Function to spawn curret items
    public void SpawnItem(string itemName)
    {
        //get name for spawn point
        spawnPoint = GameObject.Find(itemName);

        //Find and spawn coal in coal storage
        if (itemName == "spawnCoal")
        {
            obj = GameObject.Find(itemName).GetComponent<SpawnController>().spawnObject;
            spawnAmmount(100);
        }

        //Find and spawn carton in carton storage
        if (itemName == "spawnCarton")
        {
            obj = GameObject.Find(itemName).GetComponent<SpawnController>().spawnObject;
            spawnAmmount(60);
        }

        //Find and spawn ingredient in box storage
        if (itemName == "spawnIngredient")
        {
            obj = GameObject.Find(itemName).GetComponent<SpawnController>().spawnObject;
            spawnAmmount(10);
        }
    }

    //Function to spawn a certain number of items
    public void spawnAmmount(int count)
    {
        for (int i = 0; i <= count; i++)
        {
            Instantiate(obj, spawnPoint.transform.position, spawnPoint.transform.rotation);
        }
    }
}
