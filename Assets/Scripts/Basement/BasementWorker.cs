﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BasementWorker : MonoBehaviour
{
    void OnTriggerEnter(Collider collider)
    {
        if (collider.tag == "coalStorage")
        {
            int lastElement = collider.gameObject.GetComponent<CoalStorage>().coals.Count - 1;
            collider.gameObject.GetComponent<CoalStorage>().coals.ElementAt(lastElement).transform.position = new Vector3((float)(transform.position.x+0.65), transform.position.y, transform.position.z);
            collider.gameObject.GetComponent<CoalStorage>().coals.ElementAt(lastElement).transform.parent = transform;
            collider.gameObject.GetComponent<CoalStorage>().coals.ElementAt(lastElement).GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
        }
    }
}
