﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Oven : MonoBehaviour
{
    [SerializeField] public Animator anim;
    int coalCount;
    float coalTime;
    GameObject productionTable;
    GameObject ovenFire;
    GameObject backerFire;

    private void Start()
    {
        coalCount = 0;
        coalTime = 10;
        productionTable = GameObject.FindGameObjectWithTag("massTable");
        ovenFire= GameObject.FindGameObjectWithTag("ovenFire");
        backerFire= GameObject.FindGameObjectWithTag("backerFire");
        
    }
    private void Update()
    {
        //Checking coals on oven - if is empty then disable fire
        if (coalCount == 0)
        {
            productionTable.SetActive(false);
            ovenFire.SetActive(false);
            backerFire.SetActive(false);
        }
        //Couter for keep fire
        if (coalTime > 0)
        {
            coalTime -= Time.deltaTime;
            if (coalTime < 4.5)
            {
                //Information for worker
                anim.SetBool("coalAlmostBurn", true);
            }
        }
        else
        {
            coalCount--;
        }
    }
    private void OnTriggerEnter(Collider collider)
    {
        if (collider.tag == "coal")
        {
            //Add coal to oven
            Destroy(collider.gameObject);
            coalTime = 10;
            coalCount++;
            //Information for worker
            anim.SetBool("coalAlmostBurn", false);
            //Enable fire
            productionTable.SetActive(true);
            ovenFire.SetActive(true);
            backerFire.SetActive(true);
        }
    }
}
