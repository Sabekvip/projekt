﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CoalStorage : MonoBehaviour
{
    [SerializeField] public Animator anim;
    public List<GameObject>coals = new List<GameObject>();
    public bool emptyCoalStorage = false;
    private void OnTriggerEnter(Collider collider)
    {
        if (collider.tag == "coal")
        {
            //Add coal to storage
            coals.Add(collider.gameObject);
            //Information for worker
            anim.SetBool("coalOnStorage", true);
            emptyCoalStorage = false;
        }

    }
    private void OnTriggerExit(Collider collider)
    {
        if (collider.tag == "coal")
        {
            //Exit coal from storage
            coals.Remove(collider.gameObject);
            if (coals.Count == 0) anim.SetBool("coalOnStorage", false);
            emptyCoalStorage = true;
        }
    }
}
