﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BowlMass : MonoBehaviour
{
    public int level;
    public float[] amount;

    [SerializeField] public Animator animWorker;
    [SerializeField] public Animator animMixer;
    void Start()
    {
        level = 1;
        amount = new float[11];
        for (int i = 0; i<=amount.Length-1; i++)
        {
            amount[i] = (float)5.35 +i*(float)0.165;
        }
        changeSize(level);
    }

    private void OnTriggerEnter(Collider collider)
    {
        if (collider.tag == "deliveryBox")
        {
            level += 3;
            animMixer.SetBool("emptyBowl", false);
            changeSize(level);
            Destroy(collider.gameObject);
            if (level > 7)
            {
                animWorker.SetBool("fullBowlLevel", true);
            }
            if (level <= 7)
            {
                animWorker.SetBool("fullBowlLevel", false);
            }          
        }
    }

    //Function to change mass level
    public void changeSize(int level)
    {
        transform.position = new Vector3(transform.position.x, amount[level], transform.position.z);
    }
}
