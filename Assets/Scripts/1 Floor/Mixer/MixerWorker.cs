﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MixerWorker : MonoBehaviour
{
    public GameObject hand;
    void OnTriggerEnter(Collider collider)
    {
        if (collider.tag == "boxStorage")
        {  
            collider.gameObject.GetComponent<BoxStorage>().boxes.ElementAt(0).transform.parent = hand.transform;
            collider.gameObject.GetComponent<BoxStorage>().boxes.ElementAt(0).GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
            collider.gameObject.GetComponent<BoxStorage>().boxes.ElementAt(0).transform.position = hand.transform.position;
        }

    }
}
