﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxStorage : MonoBehaviour
{
    [SerializeField] public Animator anim;
    public List<GameObject> boxes = new List<GameObject>();
    public bool emptyBoxStorage;

    private void OnTriggerEnter(Collider collider)
    {
        if (collider.tag == "deliveryBox")
        {
            //Add box to storage
            boxes.Add(collider.gameObject);

            //Informations for worker
            anim.SetBool("boxOnStorage", true);
            emptyBoxStorage = false;
        }
    }
    private void OnTriggerExit(Collider collider)
    {
        if (collider.tag == "deliveryBox")
        {
            //Exti box from storage
            boxes.Remove(collider.gameObject);

            //Checking empty storage
            if (boxes.Count == 0)
            {
                anim.SetBool("boxOnStorage", false); }
                emptyBoxStorage = true;
            }
    }
}
