﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Thrust : MonoBehaviour
{
    public GameObject prefab;
    public Transform spawnPoint;
    private void OnTriggerEnter(Collider collider)
    {
        if (collider.tag == "carton")
        {
            Destroy(collider.gameObject);
            Instantiate(prefab, spawnPoint.position, spawnPoint.rotation);
        }
    }
}
