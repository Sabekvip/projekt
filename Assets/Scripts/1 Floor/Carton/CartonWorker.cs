﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CartonWorker : MonoBehaviour
{
    public GameObject hand;
    public bool inHand;
    private void OnTriggerEnter(Collider collider)
    {
        if (collider.tag == "tableCarton")
        {
            int lastElement = collider.gameObject.GetComponent<CartonTable>().cartons.Count - 1;
            collider.gameObject.GetComponent<CartonTable>().cartons.ElementAt(lastElement).transform.position = hand.transform.position;
            collider.gameObject.GetComponent<CartonTable>().cartons.ElementAt(lastElement).transform.parent = hand.transform;
            collider.gameObject.GetComponent<CartonTable>().cartons.ElementAt(lastElement).GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
            inHand = true;
        }
    }
}
