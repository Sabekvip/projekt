﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CartonTable : MonoBehaviour
{

    [SerializeField] public Animator anim;
    public List<GameObject> cartons = new List<GameObject>();
    public bool emptyCartonTable=false;
    private void OnTriggerEnter(Collider collider)
    {
        if (collider.tag == "carton")
        {
            cartons.Add(collider.gameObject);
            anim.SetBool("cartonOnTable", true);
            emptyCartonTable = false;
        }
    }
    private void OnTriggerExit(Collider collider)
    {
        if (collider.tag == "carton")
        {
            cartons.Remove(collider.gameObject);
            if (cartons.Count == 0)
            {
                anim.SetBool("cartonOnTable", false);
                emptyCartonTable = true;
            }
        }
    }
}
