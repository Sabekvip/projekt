﻿using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using UnityEngine;

public class Cookie : MonoBehaviour
{
    public int cookieLvl;
    public double points;

    void Start()
    {

        cookieLvl = Base.instance.itemList[0].lvl;
        Base.instance.itemList[0].updateCost = Base.instance.itemList[0].lvl* Base.instance.itemList[0].multipler;
        points = cookieLvl * 2 + cookieLvl * 1.1;

    }

}
