﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Backer : MonoBehaviour
{
    public Material backedCookie;
    //Table with object components
    Renderer[] childrens;

    void OnTriggerEnter(Collider collider)
    {
        if (collider.tag == "Cookie")
        {
            //Get all object components
            childrens = collider.GetComponentsInChildren<Renderer>();
            //Change meterial for cookie component
            childrens[2].material = backedCookie;
        }
    }
}
