﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Band : MonoBehaviour
{

    double pos;
    Vector3 changePosition;

    void OnTriggerEnter(Collider collider)
    {
        //Move cookie
        if (collider.tag == "Cookie")
        {
            pos =  0.05;
            changePosition = new Vector3(0,0 , -(float)pos);
        }
        //Move package
        if(collider.tag == "package")
        {
            pos = 0.03;
            changePosition = new Vector3(-(float)pos, 0, 0);
        }
    }

    void OnTriggerStay(Collider collider)
    {
        if (collider.tag == "Cookie" || collider.tag == "package")
        {
            collider.transform.position += changePosition;
        }
    }
}
