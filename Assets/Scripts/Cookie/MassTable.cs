﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MassTable : MonoBehaviour
{
    Vector3 changeScale;
    double changeSpeed;
    float timer;
    public Transform SpawnPoint;
    public GameObject prefab;
    public bool massOnTable;


    // Start is called before the first frame update

    private void OnTriggerEnter(Collider collider)
    {
        if (collider.tag == "mass")
        {
            massOnTable = true;
            timer = 0;
            changeSpeed = 0.001;
            changeScale = new Vector3((float)changeSpeed, (float)(changeSpeed*2.3), (float)changeSpeed);
        }
    }
    private void OnTriggerStay(Collider collider)
    {
        if (collider.tag == "mass")
        {
            collider.transform.localScale -= changeScale;

            timer += Time.deltaTime;
            //Spawning objects by workers level during the timer
            if (timer >= 1 - (Base.instance.itemList[1].lvl * 0.05))
            {
                SpawnPrefab();
                timer = 0;
            }
            //Check min. mass size and destroy object
            if (collider.transform.localScale.x < 0.001)
            {
                massOnTable = false;
                Destroy(collider.gameObject);

            }
        }
    }

    //Function to spawn objects (cookies) on band
    void SpawnPrefab()
    {
        Instantiate(prefab, SpawnPoint.position, SpawnPoint.rotation);
    }
}
